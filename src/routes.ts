import {Routes} from '@angular/router';

import { SignupComponent } from './app/signup/signup.component';
import { LoginComponent } from './app/login/login.component';
import { DashboardComponent } from './app/expensetracker/dashboard/dashboard.component';
import { ExpensetrackerComponent } from './app/expensetracker/expensetracker.component';
import { DetailGridComponent } from './app/expensetracker/detail-grid/detail-grid.component';
import { WeeklyGridComponent } from './app/expensetracker/weekly-grid/weekly-grid.component';
import { MonthlyGridComponent } from './app/expensetracker/monthly-grid/monthly-grid.component';
import { CategoryComponent } from './app/expensetracker/category/category.component';
import { PaymentTypesComponent } from './app/expensetracker/payment-types/payment-types.component';
import { CreditsComponent } from './app/expensetracker/credits/credits.component';
import { AuthGuard } from './app/_servics/auth.guard';

export const appRoutes: Routes = [
    {path:'', redirectTo:'/login', pathMatch:'full'},
    {path:'signup', component:SignupComponent},
    {path:'login', component:LoginComponent},
    {
      path:'',
      runGuardsAndResolvers:'always',
      canActivate:[AuthGuard],
      children:[
        {path:'expensetracker', component: ExpensetrackerComponent,
        children:[
           {path:'dashboard', component: DashboardComponent},
           {path:'detailgrid', component: DetailGridComponent},
           {path:'weelygrid', component:WeeklyGridComponent},
           {path:'monthlygrid', component:MonthlyGridComponent},
           {path:'categories', component:CategoryComponent},
           {path:'paymenttypes', component:PaymentTypesComponent},
           {path:'credits', component:CreditsComponent}
     ]},
      ]
    },

    // {path:'expensetracker/:dashboard', component: DashboardComponent},
    // {path:'expensetracker/:detailgrid', component: DetailGridComponent},
    // {path:'expensetracker/:weelygrid', component:WeeklyGridComponent},
    //{path:'expensetracker/:monthlygrid', component:MonthlyGridComponent},

];
