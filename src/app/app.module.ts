import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { appRoutes } from 'src/routes';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { DashboardComponent } from './expensetracker/dashboard/dashboard.component';
import { ExpensetrackerComponent } from './expensetracker/expensetracker.component';
import { AuthService } from './_servics/auth.service';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { DetailGridComponent } from './expensetracker/detail-grid/detail-grid.component';
import { WeeklyGridComponent } from './expensetracker/weekly-grid/weekly-grid.component';
import { MonthlyGridComponent } from './expensetracker/monthly-grid/monthly-grid.component';
import { DatePipe } from '@angular/common';
import { CategoryComponent } from './expensetracker/category/category.component';
import { PaymentTypesComponent } from './expensetracker/payment-types/payment-types.component';
import { CreditsComponent } from './expensetracker/credits/credits.component';
import { HighchartsChartModule } from 'highcharts-angular';
import { AuthGuard } from './_servics/auth.guard';
import { NgxSpinnerModule } from 'ngx-spinner';
import { LoadingInterceptor } from './_Interceptors/loading.interceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    LoginComponent,
    NavbarComponent,
    DashboardComponent,
    ExpensetrackerComponent,
    DetailGridComponent,
    WeeklyGridComponent,
    MonthlyGridComponent,
    CategoryComponent,
    PaymentTypesComponent,
    CreditsComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes, {useHash: true}),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HighchartsChartModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    })
    // ToastrModule.forRoot()
  ],
  providers: [AuthService, AuthGuard,
  {provide: HTTP_INTERCEPTORS, useClass:LoadingInterceptor, multi:true}
],
  bootstrap: [AppComponent]
})
export class AppModule { }
