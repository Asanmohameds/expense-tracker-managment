import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './_servics/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ExpenseTrackerUI';

  constructor(private router: Router,
              private authService: AuthService) {

    debugger;
    this.authService.setUserLoging();

    if(!this.authService.displayUserName) {
      this.router.navigate(['login'])
    }
  }

}

