import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../_servics/auth.service';
import { IUserForLogIn } from '../_model/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userSubmitted = false;
  user$:any[] = [];

  constructor(private fb: FormBuilder, private router: Router,
              private authService: AuthService,
              private toastrService: ToastrService) { }

  logInForm = this.fb.group({
    userName: ['', [Validators.required, Validators.minLength(4)]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });

  ngOnInit(): void {

    if(this.authService.displayUserName) {

      this.router.navigate(['expensetracker'])
    }
  }

  get userName() {
    return this.logInForm.get('userName') as FormControl;
  }

  get password() {
    return this.logInForm.get('password') as FormControl;
  }



  loginSubmit(){
    console.log(this.logInForm.value);

    this.authService.logInUser(this.logInForm.value).subscribe((response:IUserForLogIn) => {
      console.log(response);debugger;
      const user = response;
      localStorage.setItem('_User', user.UserName);
      //alert("LogIn Successfully");
      this.toastrService.success("LogIn Successfully");

      this.authService.setUserLoging();

      this.router.navigate(['expensetracker/dashboard'])
    }, error => {
      console.log(error)
      this.toastrService.error(error.error.ExceptionMessage);
    });

  }

}
