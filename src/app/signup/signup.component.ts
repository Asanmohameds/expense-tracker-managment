import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../_servics/auth.service';
import { IUser } from '../_model/user';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  userSubmitted = false;

  user: IUser;

  constructor(private fb: FormBuilder, private router:Router,
    private authService:AuthService, private toastrService: ToastrService) { }

  ngOnInit(): void {

    if(this.authService.displayUserName)
    {
      this.router.navigate(['expensetracker'])
    }

  }

  //Forms declaration:

  signUpForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(6)]],
    confirmPassword: ['', [Validators.required, Validators.minLength(6)]],
    Name: ['', [Validators.required, Validators.minLength(4)]],
    userName: ['', [Validators.required, Validators.minLength(4)]],
    mobileNumber: ['', [Validators.required, Validators.maxLength(10)]]
  },  {validators: this.passwordMatchingValidator});

  passwordMatchingValidator (group:FormGroup) {
    return group.get('password')?.value === group.get('confirmPassword')?.value ? null :  { notmatched: true }
  }

  get email() {
    return this.signUpForm.get('email') as FormControl;
  }

  get password() {
    return this.signUpForm.get('password') as FormControl;
  }

  get confirmPassword() {
    return this.signUpForm.get('confirmPassword') as FormControl;
  }

  get userName() {
    return this.signUpForm.get("userName") as FormControl;
  }

  get Name() {
    return this.signUpForm.get("Name") as FormControl;
  }

  get mobileNumber() {
    return this.signUpForm.get("mobileNumber") as FormControl;
  }

  signupSubmit() {
    console.log(this.signUpForm.value);
    this.userSubmitted = true;
    if(this.signUpForm.valid){
      // this.user = Object.assign(this.user, this.signUpForm.value);
      this.userData();
      this.authService.addUsers(this.user).subscribe((x) => {
        console.log(x);
        //alert("Registerd Successfully");
        this.toastrService.success("Registerd Successfully")
        this.router.navigate(['login']);
      }, error => {
        console.log(error);
        this.toastrService.error(error.error.ExceptionMessage)
      });
      this.signUpForm.reset();
      this.userSubmitted = false;
    }
    }


    // ***** Get data from form using getter method and store it the way we wanted.
  userData() : IUser {

    return this.user = {
      userName: this.userName.value,
      email: this.email.value,
      password: this.password.value,
      Name: this.Name.value,
      Phone_Number:this.mobileNumber.value
    }
  }



}
