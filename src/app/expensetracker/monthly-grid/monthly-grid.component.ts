import { Component, OnInit } from '@angular/core';
import { IExpenseVM } from 'src/app/_model/expense';
import { CommonService } from 'src/app/_servics/common.service';

@Component({
  selector: 'app-monthly-grid',
  templateUrl: './monthly-grid.component.html',
  styleUrls: ['./monthly-grid.component.css']
})
export class MonthlyGridComponent implements OnInit {

  expenseData: IExpenseVM[] = [];
  public sumOfMonthly:number;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {

    this.retriveData();
    this.getMonthlyTotal();
  }

   // To recive data from DB to table.
   retriveData() {
    debugger;
      this.commonService.getMonthlyAllExpensesOfUser().subscribe((x:IExpenseVM[]) => {
      console.log(x);
      this.expenseData = x;
    })
  }


      getMonthlyTotal(){
         this.commonService.getSumOfMonthlyExp().subscribe((x:any) => {
          debugger;
          this.sumOfMonthly = x.totalExp;
        })
      }


}
