import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ICategoriesVM } from 'src/app/_model/categories';
import { CategoriesService } from 'src/app/_servics/categories.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  userSubmitted = false;
  isPopOpen = false;
  isEdit = false;
  categoriesData: ICategoriesVM[] = [];

  categoryName:ICategoriesVM;

  constructor(private fb: FormBuilder, public categoryService: CategoriesService) {
    this.retriveData();
  }

  ngOnInit(): void {
  }

  categoryForm = this.fb.group({
    rec_iD:[null,],
    expCategory:[null, Validators.required],

  })

  get rec_iD() {
    return this.categoryForm.get('rec_iD') as FormControl;
  }
  get expCategory() {
    return this.categoryForm.get('expCategory') as FormControl;
  }

  data(): ICategoriesVM {

    return this.categoryName = {
    RecId: this.rec_iD.value,
    CategoryName: this.expCategory.value

    }
  }

  retriveData() {
    this.categoryService.getAllCategories().subscribe((x:ICategoriesVM[]) => {
      console.log(x);
      this.categoriesData = x;
    })
  }


  createData() {
    this.isPopOpen = true;
    this.isEdit = false;
    this.data();
  }

  closePop() {
    this.isPopOpen = false;
    this.categoryForm.reset();
  }

  submitData() {

    this.userSubmitted = true;
    if(this.categoryForm.valid){
      if(this.data().RecId == null || this.data().RecId == undefined && (!this.editData)){
        this.categoryService.addCategory(this.data()).subscribe((x:ICategoriesVM) => {
          console.log(x);
          alert("Added Successfully");
          this.retriveData();
        })
      } else {
        this.categoryService.updateCategory(this.data()).subscribe((x:ICategoriesVM) => {
          console.log(x);
          alert("Updated Successfully");
          this.retriveData();
        })
      }
    }

  }

  editData(data:ICategoriesVM) {

    this.isPopOpen = true;
    this.isEdit = true;

    let forObj = {
      rec_iD:data.RecId,
      expCategory:data.CategoryName

    }
    this.categoryForm.patchValue(forObj);


  }

  deleteData() {


  }




}
