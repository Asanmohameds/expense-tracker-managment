import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ICategoriesVM } from 'src/app/_model/categories';
import { IExpenseVM } from 'src/app/_model/expense';
import { IPaymentTypesCVM } from 'src/app/_model/paymentTypes';
import { CategoriesService } from 'src/app/_servics/categories.service';
import { ExpenseService } from 'src/app/_servics/expense.service';
import { PaymentTypesService } from 'src/app/_servics/payment-types.service';

@Component({
  selector: 'app-detail-grid',
  templateUrl: './detail-grid.component.html',
  styleUrls: ['./detail-grid.component.css']
})
export class DetailGridComponent implements OnInit {

  // maxDate;

  userSubmitted = false;
  isPopOpen = false;
  isEdit = false;
  expenseData: IExpenseVM[] = [];

  userExpense:IExpenseVM;


  constructor(private fb: FormBuilder, private expenseService:ExpenseService,
    public categoryService: CategoriesService, public paymentService: PaymentTypesService,
    private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.retriveData();
    // this.maxDate = new Date();
    // this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
  }

  expenseForm = this.fb.group({
    rec_iD:[null,],
    user_Id:[''],
    expName:[null, Validators.required],
    expAmount:[null, Validators.required],
    expCategory:[null, Validators.required],
    ePaymentTypeId:[null, Validators.required],
    expDesc:[null, Validators.required],
    expdate:[null, Validators.required]
  })

  get rec_iD() {
    return this.expenseForm.get('rec_iD') as FormControl;
  }

  get user_Id() {
    return this.expenseForm.get('user_Id') as FormControl;
  }
  get expName() {
    return this.expenseForm.get('expName') as FormControl;
  }
  get expAmount() {
    return this.expenseForm.get('expAmount') as FormControl;
  }
  get expCategory() {
    return this.expenseForm.get('expCategory') as FormControl;
  }

  get ePaymentTypeId() {
    return this.expenseForm.get('ePaymentTypeId') as FormControl;
  }
  get expDesc() {
    return this.expenseForm.get('expDesc') as FormControl;
  }
  get expdate() {
    return this.expenseForm.get('expdate') as FormControl;
  }

  submitData(){
    this.userSubmitted = true;

    if(this.expenseForm.valid){
      if(this.data().RecId == null || this.data().RecId == undefined && (!this.editData)){
        this.expenseService.addExpense(this.data()).subscribe((x:IExpenseVM) => {
          console.log(x);
          //alert("Added expense data successfully");
          this.toastrService.success("Added Expense data Successfully");
          //Once submit data, data has to store in local also view in table/userview.
          this.retriveData();
        },
        (error) => {
          alert(error);
          this.toastrService.error(error.error.ExceptionMessage);

        })
      }
      else{

          this.expenseService.updateExpense(this.data()).subscribe((x:IExpenseVM) => {
            console.log(x);
            //alert("Updated Successfully");
            this.toastrService.success("Updated Successfully");
            //Once submit data, data has to store in local also view in table/userview.
            this.retriveData();
          },
          (error) => {
            alert(error);
            this.toastrService.error(error.error.ExceptionMessage);
          })
      }

      this.closePop();
      //Once submit data, form has to reset to empty.
      this.expenseForm.reset();
      this.userSubmitted = false;
    }
  }

  // To recive data from DB to table.
  retriveData() {
    debugger;
    this.expenseService.getAllExpensesOfUserQuery().subscribe((x:IExpenseVM[]) => {
      console.log(x);
      this.expenseData = x;
    })
  }

  createData() {
    this.isPopOpen = true;
    this.isEdit = false;
    this.getAllCategories();
    this.getAllPayments();
    this.data();
  }

  // ***** Get data from form using getter method and store it the way we wanted.
  data(): IExpenseVM {

    return this.userExpense = {
      RecId:this.rec_iD.value,
      UserID: this.user_Id.value,
      ExpenseName: this.expName.value,
      ExpenseAmount: this.expAmount.value,
      ExpenseCategory: this.expCategory.value,
      ExpensePayment_TypeId: this.ePaymentTypeId.value,
      ExpenseNotes: this.expDesc.value,
      Date: this.expdate.value
    }
  }


  // First create form data, once modal opened.
    closePop() {
      this.isPopOpen = false;
      this.expenseForm.reset();
    }

    editData(data: IExpenseVM) {
      debugger;
      this.isPopOpen = true;
      this.isEdit=true;
      debugger;
      this.getAllCategories();
      this.getAllPayments();


     let datestr = data.Date.split('T')[0]; //2021-04-01

      // *********This is another method to get the data format...

      //   let _date = new Date(data.Date); // format should be YYYY-MM-DD /2021-04-01
      //  let datestr1 = _date.getFullYear()+"-"
      //  + ( _date.getMonth() > 8? (_date.getMonth()+1) : ("0"+(_date.getMonth()+1))  )
      //  +"-"
      //  + ( _date.getDate() > 9? (_date.getDate()) : ("0"+(_date.getDate()))  )

      //  alert("datestr-"+datestr +"\n datestr1-"+datestr1);

      let forObj = {
        rec_iD:data.RecId,
        user_Id:data.UserID,
        expName:data.ExpenseName,
        expAmount:data.ExpenseAmount,
        expCategory:data.ExpenseCategory,
        ePaymentTypeId:data.ExpensePayment_TypeId,
        expDesc:data.ExpenseNotes,
        expdate:datestr

      }
      this.expenseForm.patchValue(forObj);
     //this.expenseForm.setValue(forObj);

    }

    deleteData(recId: number) {
debugger;
      if(!confirm("Are you sure want to delete?"))
      return;
      this.expenseService.deleteExpense(recId).subscribe(
        (res =>{
          console.log(res);
          this.retriveData();
        }),
        (error =>{
          console.log(error);
        })
      );

    }

    categoriesData: ICategoriesVM[] = [];
    getAllCategories() {
      this.categoryService.getAllCategories().subscribe((x:ICategoriesVM[]) => {
        console.log(x);
        this.categoriesData = x;
      }, (error) => {
        alert(error);
      })

    }

    paymentTypesData: IPaymentTypesCVM[] = [];
    getAllPayments(){
      this.paymentService.getAllPaymentTypes().subscribe((x:IPaymentTypesCVM[]) => {
        this.paymentTypesData = x;
      }, (error) => {
        alert(error);
      })
    }


}
