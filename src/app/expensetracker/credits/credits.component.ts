import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ICreditsCVM } from 'src/app/_model/credits';
import { CreditsService } from 'src/app/_servics/credits.service';

@Component({
  selector: 'app-credits',
  templateUrl: './credits.component.html',
  styleUrls: ['./credits.component.css']
})
export class CreditsComponent implements OnInit {

  userSubmitted = false;
  isPopOpen = false;
  isEdit = false;
  creditsData: ICreditsCVM[] = [];

  _creditType:ICreditsCVM;

  constructor(private fb: FormBuilder, private creditsService: CreditsService) { }

  ngOnInit(): void {

    this.retriveData();
  }

  creditsForm = this.fb.group({
    rec_iD:[null,],
    creditDate:[null, Validators.required],
    creditType:[null, Validators.required],
    creditAmount:[null, Validators.required],


  })

  get rec_iD() {
    return this.creditsForm.get('rec_iD') as FormControl;
  }
  get creditDate() {
    return this.creditsForm.get('creditDate') as FormControl;
  }

  get creditType() {
    return this.creditsForm.get('creditType') as FormControl;
  }

  get creditAmount() {
    return this.creditsForm.get('creditAmount') as FormControl;
  }

  data(): ICreditsCVM {

    return this._creditType = {
    RecId: this.rec_iD.value,
    Date: this.creditDate.value,
    CreditType: this.creditType.value,
    Amount: this.creditAmount.value
    }
  }

  retriveData() {
    this.creditsService.getAllCredits().subscribe((x:ICreditsCVM[]) => {
      console.log(x);

      this.creditsData = x
    })
  }

  createData() {
    this.isPopOpen = true;
    this.isEdit = false;
    this.data();

  }

  closePop() {
    this.isPopOpen = false;
    this.creditsForm.reset();
  }

  submitData() {
    this.userSubmitted = true;
    if(this.creditsForm.valid){
      if(this.data().RecId == null || this.data().RecId == undefined && (!this.editData)){
        this.creditsService.addCredits(this.data()).subscribe((x:ICreditsCVM) => {
          console.log(x);
          alert("Added Successfully");
          this.retriveData();
          this.closePop();
        }, (error) => {
          alert(error);
        })
      } else {
        this.creditsService.updateCredits(this.data()).subscribe((x:ICreditsCVM) => {
          console.log(x);
          alert("Updated Successfully");
          this.retriveData();
          this.closePop();
        }, (error) => {
          alert(error);
        })
      }
    }
  }

  editData(data: ICreditsCVM){
    this.isPopOpen = true;
    this.isEdit = true;

    let forObj = {
      rec_iD:data.RecId,
      creditDate:data.Date,
      creditType:data.CreditType,
      creditAmount:data.Amount
    }

    this.creditsForm.patchValue(forObj);
  }

  deleteData() {

  }

}
