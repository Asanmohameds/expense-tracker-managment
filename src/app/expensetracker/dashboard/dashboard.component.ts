import { Component, OnInit } from '@angular/core';

import * as Highcharts from 'highcharts';
import { CommonService } from 'src/app/_servics/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {
    this.getExpanceChartData();
    this.getCreditChartData();
  }

  expData:any[] =[ ];
  creditdata:any[]=[ ];

  Highcharts: typeof Highcharts = Highcharts;
  chartOptions: Highcharts.Options =null;

  Expensescharts() {

    this.chartOptions = {
        title: {
          text: 'Expenses Catagory'
        },
      chart: {
        animation:true,
      },

      series: [{
      name: 'Expenses Catagory',
      data: this.expData,
      type:"pie",
      showInLegend: true
      }]
    };
  }

  Highcharts1: typeof Highcharts = Highcharts;
  chartOptions1: Highcharts.Options =null;

  CreditChart() {
    this.chartOptions1 = {
      title: {
        text: 'Credits Types'
      },
    chart: {
      animation:true,
    },

    series: [{
    name: 'Credits Types',
    data: this.creditdata,
    type:"pie",
    showInLegend: true
    }]
  };
  }

  getExpanceChartData(){
    this.commonService.getSumOfParticularCategoryExpensesOfUser().subscribe((x:any) => {
     debugger;
     this.expData =[];
     x.forEach(element => {
        let obj ={
          name:element.CategoryName,
          y:element.Amount,
          //z: Math.floor((Math.random() * 100) + 1),
        }
        this.expData.push(obj);
     });

     this.Expensescharts();

   })
 }

 getCreditChartData(){
  this.commonService.getSumOfParticularCreditExpensesOfUser().subscribe((x:any) => {
   debugger;
   this.creditdata =[];
   x.forEach(element => {
      let obj ={
        name:element.CreditType,
        y:element.Amount,
        //z: Math.floor((Math.random() * 100) + 1),
      }
      this.creditdata.push(obj);
   });

   this.CreditChart();

 })
}


}
