import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { IPaymentTypesCVM } from 'src/app/_model/paymentTypes';
import { PaymentTypesService } from 'src/app/_servics/payment-types.service';

@Component({
  selector: 'app-payment-types',
  templateUrl: './payment-types.component.html',
  styleUrls: ['./payment-types.component.css']
})
export class PaymentTypesComponent implements OnInit {

  userSubmitted = false;
  isPopOpen = false;
  isEdit = false;
  paymentTypesData: IPaymentTypesCVM[] = [];

  _paymentTypes:IPaymentTypesCVM;

  constructor(private fb: FormBuilder, private paymentService: PaymentTypesService) {

  }

  ngOnInit(): void {
    this.retriveData();
  }

  paymentTypesForm = this.fb.group({
    rec_iD:[null,],
    pTypes:[null, Validators.required],

  })

  get rec_iD() {
    return this.paymentTypesForm.get('rec_iD') as FormControl;
  }
  get pTypes() {
    return this.paymentTypesForm.get('pTypes') as FormControl;
  }

  data(): IPaymentTypesCVM {

    return this._paymentTypes = {
    RecId: this.rec_iD.value,
    PaymentType: this.pTypes.value

    }
  }
  createData() {
    this.isPopOpen = true;
    this.isEdit = false;
    this.data();
  }

  submitData() {

    this.userSubmitted = true;
    if(this.paymentTypesForm.valid){
      if(this.data().RecId == null || this.data().RecId == undefined && (!this.editData)){
        this.paymentService.addPaymentType(this.data()).subscribe((x:IPaymentTypesCVM) => {
          console.log(x);
          alert("Added Successfully");
          this.closePop();
          this.retriveData();
        })
      } else {
        this.paymentService.updatePaymentType(this.data()).subscribe((x:IPaymentTypesCVM) => {
          console.log(x);
          alert("Updated Successfully");
          this.retriveData();
        })
      }
    }
  }

  retriveData() {
    debugger;
    return this.paymentService.getAllPaymentTypes().subscribe((x:IPaymentTypesCVM[]) => {
      // console.log(x);
      this.paymentTypesData=x;
    })
  }

  editData(data:IPaymentTypesCVM) {

    this.isPopOpen = true;
    this.isEdit = true;

    let forObj = {
      rec_iD:data.RecId,
      pTypes:data.PaymentType

    }
    this.paymentTypesForm.patchValue(forObj);
  }

  deleteData() {

  }

  closePop(){
    this.isPopOpen = false;
    this.paymentTypesForm.reset();
  }



}
