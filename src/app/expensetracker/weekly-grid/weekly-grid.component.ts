import { Component, OnInit } from '@angular/core';
import { IExpenseVM } from 'src/app/_model/expense';
import { CommonService } from 'src/app/_servics/common.service';


@Component({
  selector: 'app-weekly-grid',
  templateUrl: './weekly-grid.component.html',
  styleUrls: ['./weekly-grid.component.css']
})
export class WeeklyGridComponent implements OnInit {

   expenseData: IExpenseVM[] = [];
   public sumOfWeekly:number;

  constructor(private commonService: CommonService) { }

  ngOnInit(): void {

    this.retriveData();
    this.getWeeklyTotal()

  }


  // To recive data from DB to table.
  retriveData() {
  debugger;
    this.commonService.getWeeklyAllExpensesOfUser().subscribe((x:IExpenseVM[]) => {
    console.log(x);
    this.expenseData = x;
  })
}


    getWeeklyTotal(){
      return this.commonService.getSumOfWeeklyExp().subscribe((x:any) => {
        debugger;
        this.sumOfWeekly = x.totalExp;
      })
    }

}
