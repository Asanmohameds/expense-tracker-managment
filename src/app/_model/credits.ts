export interface ICreditsCVM {

  RecId:number;
  Date: string | null;
  CreditType: string;
  Amount: number;
}
