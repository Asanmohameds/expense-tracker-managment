export interface IUser {
  userName? : string;
  Name? : string;
  password? : string;
  email?: string;
  Phone_Number?: string;
  Image?: string;
}


export interface IUserForLogIn {
  UserName : string;
  password : string;
  RecId?: string;

}
