export interface IExpenseVM {
  RecId: number;
  UserID?: number;
  ExpenseName? : string;
  ExpenseAmount? : number;
  ExpenseNotes? : string;
  Date?: any;
  ExpenseCategory?: number;
  ExpensePayment_TypeId?: number;
  CategoryName? : string;
  PaymentType? : string;
}

export interface IExpenseUVM {
  RecId: number;
  UserID?: number;
  ExpenseName? : string;
  ExpenseAmount? : number;
  ExpenseNotes? : string;
  Date?: Date;
  ReqDate?: Date;
  ExpenseCategory?: number;
  ExpensePayment_TypeId?: number;
}


export interface ICreditsVM {
  date: string | null;
  creditType: string;
  amount: number;
  RecId: number;
}
