import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICategoriesVM } from '../_model/categories';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  baseUrl = 'http://localhost:53535';

  constructor(private http: HttpClient, private authService: AuthService) { }


  getAllCategories() {
    return this.http.get(this.baseUrl + '/api/all/categories');
 }

 addCategory(formData: ICategoriesVM){
   return this.http.post(this.baseUrl + '/api/add/category', formData);
 }

 updateCategory(formDateId:ICategoriesVM){
   debugger;
     return this.http.put(this.baseUrl + '/api/modify/category/WithId?Id='+formDateId.RecId, formDateId)
 }

 deleteCategory(formDataId: any){
   return this.http.delete(this.baseUrl + '/api/delete/categoryId', formDataId);
 }



}
