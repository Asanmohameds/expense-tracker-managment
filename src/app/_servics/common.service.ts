import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IExpenseVM, IExpenseUVM } from '../_model/expense';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  baseUrl = 'http://localhost:53535/';

  constructor(private http:HttpClient, private authService: AuthService) { }

  getHttpheader() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'UserName':this.authService.displayUserName
      });

    let options = { headers: headers };
    return options;
   }

   //To get today's all expenses of particular user.
   getTodayAllExpensesOfUser() {
    return this.http.get(this.baseUrl + 'api/today/all/expenses/user',this.getHttpheader());
 }

   //To get Weekly all expenses of particular user.
   getWeeklyAllExpensesOfUser() {
    return this.http.get(this.baseUrl + 'api/weekly/all/expenses/user',this.getHttpheader());
 }

  //To get Monthly all expenses of particular user.
  getMonthlyAllExpensesOfUser() {
    return this.http.get(this.baseUrl + 'api/monthly/all/expenses/user',this.getHttpheader());
 }

  //Sum of expense Amount of particular user's in data..
  getSumOfAllExpensesOfUser() {
    return this.http.get(this.baseUrl + 'api/sum/all/expenses/user',this.getHttpheader());
 }

 //Sum of the particular category records expenses of user's for dashboard
 getSumOfParticularCategoryExpensesOfUser() {
  return this.http.get(this.baseUrl + 'api/sum/all/category/expenses/user',this.getHttpheader());
}

//Sum of the particular credits records expenses of user's for dashboard
getSumOfParticularCreditExpensesOfUser() {
  return this.http.get(this.baseUrl + 'api/sum/all/credits/expenses/user',this.getHttpheader());
}


//Sum of WEEKLY expense Amount of particular user's in data

getSumOfWeeklyExp(){
  return this.http.get(this.baseUrl + 'api/sum/weekly/all/expenses/user',this.getHttpheader());
}



//Sum of MONTHLY expense Amount of particular user's in data

getSumOfMonthlyExp(){
  return this.http.get(this.baseUrl + 'api/sum/monthly/all/expenses/user',this.getHttpheader());
}





}
