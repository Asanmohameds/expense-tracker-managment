import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IExpenseVM, IExpenseUVM } from '../_model/expense';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {

  baseUrl = 'http://localhost:53535';

  constructor(private http:HttpClient, private authService: AuthService) { }

   getHttpheader() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'UserName':this.authService.displayUserName
      });

    let options = { headers: headers };
    return options;
   }


  getAllExpensesOfUser() {
     return this.http.get(this.baseUrl + '/api/all/expenses/user',this.getHttpheader());

  }

  getAllExpensesOfUserQuery() {
    return this.http.get(this.baseUrl + '/api/all/expenses/user/query',this.getHttpheader());

 }

  addExpense(formData: IExpenseVM){
    return this.http.post(this.baseUrl + '/api/expenses/create', formData,this.getHttpheader());
  }

  updateExpense(formDateId:IExpenseVM){
    debugger;
      return this.http.put(this.baseUrl + '/api/modifyexpenseWithId?Id='+formDateId.RecId, formDateId,this.getHttpheader())
  }

  deleteExpense(formDataId: number){
    return this.http.delete(this.baseUrl + `/api/delete/expenseId/${formDataId}`,this.getHttpheader());
  }





}
