import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { IUser, IUserForLogIn } from '../_model/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  baseUrl = 'http://localhost:53535';
  registerBaseUrl = 'http://localhost:53535/api/Registers/user';

  public displayLogIn: boolean = true;
  public displaySignUp: boolean = true;
  public displayLogOut: boolean = false;
  public displayUserName: string | undefined = "";

  constructor(private http:HttpClient) { }


  addUsers(user: IUser){
    return  this.http.post(this.registerBaseUrl, user);
  }

  logInUser(user: IUserForLogIn){
    return this.http.post(this.baseUrl + '/api/login/user', user);
  }

  setUserLoging() {

   this.displayUserName = localStorage.getItem('_User');
    if(this.displayUserName) {
      this.displaySignUp=false;
      this.displayLogIn=false;
      this.displayLogOut=true;
}
  }


  logOut() {
    localStorage.removeItem('_User')
    this.displayUserName = null;
    this.displayLogOut = false;
    this.displayLogIn = true;
    this.displaySignUp = true;
  }


}
