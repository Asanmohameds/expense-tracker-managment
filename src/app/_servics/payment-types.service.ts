import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPaymentTypesCVM } from '../_model/paymentTypes';

@Injectable({
  providedIn: 'root'
})
export class PaymentTypesService {

  baseUrl = 'http://localhost:53535';

  constructor(private http: HttpClient) { }


  getAllPaymentTypes() {
    return this.http.get(this.baseUrl + '/api/all/PaymentTypes');
 }

 addPaymentType(formData: IPaymentTypesCVM){
   return this.http.post(this.baseUrl + '/api/add/paymentType', formData);
 }

 updatePaymentType(formDateId:IPaymentTypesCVM){
   debugger;
     return this.http.put(this.baseUrl + '/api/modify/paymentType/WithId?Id='+formDateId.RecId, formDateId)
 }

 deletePaymentType(formDataId: any){
   return this.http.delete(this.baseUrl + '/api/delete/paymentTypeId', formDataId);
 }

}
