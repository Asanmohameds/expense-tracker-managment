import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ICreditsCVM } from '../_model/credits';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CreditsService {

  baseUrl = 'http://localhost:53535';

  constructor(private http: HttpClient, private authService: AuthService) { }

  getHttpheader() {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'UserName':this.authService.displayUserName
      });

    let options = { headers: headers };
    return options;
   }

  getAllCredits() {
    return this.http.get(this.baseUrl + '/api/all/credits', this.getHttpheader());
 }

 addCredits(formData: ICreditsCVM){
   return this.http.post(this.baseUrl + '/api/add/credit', formData, this.getHttpheader());
 }

 updateCredits(formDateId:ICreditsCVM){
   debugger;
     return this.http.put(this.baseUrl + '/api/modify/credit/WithId?Id='+formDateId.RecId, formDateId, this.getHttpheader())
 }

 deleteCredits(formDataId: any){
   return this.http.delete(this.baseUrl + '/api/delete/creditId', formDataId);
 }

}
